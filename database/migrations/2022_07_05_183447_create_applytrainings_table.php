<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplytrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applytrainings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('approval_id');
            $table->foreignId('company_id');
            $table->string('name');
            $table->text('deskripsi');
            $table->date('first_date');
            $table->date('last_date');
            $table->string('prove_pic')->nullable();
            $table->integer('price');
            $table->string('location');
            $table->string('link');
            $table->string('note')->nullable();
            $table->string('certificate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applytrainings');
    }
}
