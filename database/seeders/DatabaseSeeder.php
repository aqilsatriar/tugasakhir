<?php

namespace Database\Seeders;

use App\Models\Approval;
use App\Models\Roles;
use App\Models\Position;
use App\Models\Company;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'roles_id' => 1,
            'name' => 'AqilAdmin',
            'email' => 'aqiladmin@gmail.com',
            'positions_id' => 2,
            'address' => 'Perumahan Megawon Indah Rt 99 RW 12',
            'place_date' => "Semarang",
            'birth_date' => "2000-12-04",
            'join_date' => "2020-10-07",
            'password' => bcrypt('12345678')
        ]);

        User::create([
            'roles_id' => 2,
            'name' => 'Aqil',
            'email' => 'aqil@gmail.com',
            'password' => bcrypt('12345678'),
            'positions_id' => 4,
            'address' => 'Perumahan Megawon Indah Rt 99 RW 12',
            'place_date' => "Semarang",
            'birth_date' => "2000-12-04",
            'join_date' => "2020-10-07",
        ]);

        User::create([
            'roles_id' => 3,
            'name' => 'AqilDirector',
            'email' => 'aqildirector@gmail.com',
            'password' => bcrypt('12345678'),
            'positions_id' => 1,
            'address' => 'Perumahan Megawon Indah Rt 99 RW 12',
            'place_date' => "Semarang",
            'birth_date' => "2000-12-04",
            'join_date' => "2020-10-07",
        ]);

        Approval::create([

            'name' => 'submitted'
        ]);
        Approval::create([

            'name' => 'accepted'

        ]);

        Approval::create([

            'name' => 'rejected'

        ]);

        Approval::create([

            'name' => 'done'

        ]);

        Roles::create([

            'roles' => 'Admin'

        ]);

        Roles::create([

            'roles' => 'Employee'

        ]);

        Roles::create([

            'roles' => 'Director'

        ]);


        Position::create([

            'name' => 'Director'

        ]);

        Position::create([

            'name' => 'HR'

        ]);
        Position::create([

            'name' => 'Project Manager'

        ]);
        Position::create([

            'name' => 'Back End Developer'

        ]);
        Position::create([

            'name' => 'Front End Developer'

        ]);
        Position::create([

            'name' => 'UI/UX Designer'

        ]);
        Position::create([

            'name' => 'Data Analyst'

        ]);
        Position::create([

            'name' => 'Other'

        ]);

        Company::create([

            'name' => 'PT. Kita Monster Digital'

        ]);
        Company::create([

            'name' => 'PT. Sucofindo'

        ]);
        Company::create([

            'name' => 'PT. Sumber Baru Rejeki'

        ]);
        Company::create([

            'name' => 'PT. Djarum Foundation'

        ]);
        Company::create([

            'name' => 'PT. Freeport Indonesia'

        ]);
        Company::create([

            'name' => 'PT. Exxon Oil'

        ]);
        Company::create([

            'name' => 'PT. PLN Indonesia'

        ]);

        // \App\Models\User::factory(10)->create();
    }
}
