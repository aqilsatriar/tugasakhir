<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\OfficerdataController;
use App\Http\Controllers\ApplytrainingController;
use App\Http\Controllers\TrainingApprovalController;
use App\Http\Controllers\TrainingHistoryController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\TrainingsubmController;
use App\Http\Controllers\OFTrainingHistoryController;
use App\Http\Controllers\MultiUserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('folder-profile.view');
});

Route::get('/login', function () {
    return view('auth.login');
});

// Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');

Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Auth::routes();

Route::get('/redirects', [MultiUserController::class, "index"]);

Route::get('/home', [App\Http\Controllers\MultiUserController::class, 'index']);

Route::get('/printadmin', [MultiUserController::class, "print_adminAll"]);

Route::get('/printadminacc', [MultiUserController::class, "print_adminAcc"]);

Route::get('/training/trainingacc', [MultiUserController::class, "tampilAccepted"]);
Route::get('/training/trainingreject', [MultiUserController::class, "tampilRejected"]);
Route::get('/training/trainingsubmission', [MultiUserController::class, "tampilSubmitted"]);
Route::get('/training/trainingdone', [MultiUserController::class, "tampilDone"]);

Route::get('/dashtraining/trainingacc', [MultiUserController::class, "dashAccepted"]);
Route::get('/dashtraining/trainingreject', [MultiUserController::class, "dashRejected"]);
Route::get('/dashtraining/trainingsubmission', [MultiUserController::class, "dashSubmitted"]);
Route::get('/dashtraining/trainingdone', [MultiUserController::class, "dashDone"]);

Route::get('/dataofficer/admin', [MultiUserController::class, "tampilAdmin"]);
Route::get('/dataofficer/employee', [MultiUserController::class, "tampilEmployee"]);
Route::get('/dataofficer/officer', [MultiUserController::class, "tampilOfficer"]);

Route::get('/trainingdownload_pdf', [MultiUserController::class, "download_pdf"]);
// Route::get('/officerdata', [OfficerdataController::class, ['TampilData']])->middleware('auth');
// Route::get('/home', function() {
//     return view('dashboard');})->middleware('auth');

Route::get('/officerdata', [OfficerdataController::class, "index"])->middleware('auth');

Route::get('/officerdata/download_pdf', [OfficerdataController::class, "cetak_pdf"]);

Route::get('/officerdata/print_pdf', [OfficerdataController::class, "print_pdf"]);

Route::post('/createofficerdata', [OfficerdataController::class, 'store']);

Route::get('/officerdata/show/{id}', [OfficerdataController::class, 'show']);

Route::get('/officerdata/edit/{id}', [OfficerdataController::class, "edit"]);

Route::put('/officerdata/edit/{id}', [OfficerdataController::class, "update"]);

Route::post('/officerdata/{id}', [OfficerdataController::class, 'destroy']);


// Route::resource('/officerdata', OfficerdataController::class)->middleware('auth');

Route::resource('/trainingapproval', TrainingApprovalController::class)->middleware('auth');

Route::resource('/traininghistory', TraininghistoryController::class)->middleware('auth');

Route::resource('/applytraining', ApplytrainingController::class)->middleware('auth');

Route::post('/createTraining', [ApplytrainingController::class, 'store']);

Route::resource('/company', CompanyController::class)->middleware('auth');

Route::get('/companyofc', [CompanyController::class, 'indexofc'])->middleware('auth');

Route::get('/companyofc/create', [CompanyController::class, 'createofc'])->middleware('auth');

Route::post('/companyofcsave', [CompanyController::class, 'storeofc'])->middleware('auth');

// Route::post('/applytraining/create', [ApplytrainingController::class, 'addCompany']);

Route::resource('/position', PositionController::class)->middleware('auth');

Route::resource('/trainingsubm', TrainingsubmController::class)->middleware('auth');

Route::get('/trainingsubm/details/{id}', [TrainingsubmController::class, 'show']);

Route::get('/traininghistory/details/{id}', [TrainingHistoryController::class, 'show']);

Route::resource('/oftraininghistory', OFTrainingHistoryController::class)->middleware('auth');

Route::get('oftraininghistory/print/{id_show}', [OFTrainingHistoryController::class, "show"]);

Route::put('updateCertificate/{id}', [OFTrainingHistoryController::class, "updateCertificate"]);

Route::get('/trainingsubm/show/{id}', [TrainingsubmController::class, 'show']);

Route::get('/trainingapproval/details/{id}', [TrainingApprovalController::class, 'show']);

Route::put('/trainingapproval/note/{id}', [TrainingApprovalController::class, 'addNote']);

Route::get('/trainingapprovalAccept/{id_apply}', [TrainingApprovalController::class, "statusAccept"]);

Route::get('/trainingapprovalDenied/{id_apply}', [TrainingApprovalController::class, "statusDeny"]);