<!DOCTYPE html>
<html>

<head>
    <title>Training Recapitulation of KitaDigi</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    <h4>
        <center>Training Recapitulation of KitaDigi<BR>
    </h4>
    <center>
        <p>Griya Edelweis blok F10, Dusun V, Joho,
            Kec. Mojolaban, Kabupaten Sukoharjo, Jawa Tengah 57554.
            E-mail : cs@kitadigi.com Website : www.kitadigi.com
        </p>
    </center>
    <hr style="width:max;height:2px;">
    <b>
    </b>
    <br>

    <table class='table table-bordered'>
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Position</th>
                <th>Training Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Company</th>
                <th>Price</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($trainings as $trn)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $trn->username }}</td>
                    <td>{{ $trn->posname }}</td>
                    <td>{{ $trn->applyname }}</td>
                    <td>{{ date('d F Y', strtotime($trn->first_date)) }}</td>
                    </h5>
                    <td>{{ date('d F Y', strtotime($trn->last_date)) }}</td>
                    <td>{{ $trn->company }}</td>
                    <td>Rp. {{ $trn->price }}</td>
                    <td>{{ $trn->name }}</td>


                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>

<script type="text/javascript">
    window.print();
</script>
