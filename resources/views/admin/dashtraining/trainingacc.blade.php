@extends('layouts.main')

@section('content-wrapper')
    <div class="container">
        <div class="card mt-3">
            <div class="card-header p-0 position-relative mt-n4 mt-3 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-light border-radius-lg pt-4 pb-3">
                    <h3 class="text-black text-capitalize ps-3" align="center">Training Accepted
                    </h3>
                    <p class="text-white text-capitalize ps-3" align="center">All Accepted Training in
                        Kita Monster Digital
                    </p>
                </div>
            </div>
            <div class="card-body px-0 pb-0 pt-3">

                <div class="card-body table-responsive pt-1">
                    <table id="myTable" class="table table-hover">
                        <thead class="text-warning">
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                No</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Name</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Role</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Training</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Date</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Status</th>
                        </thead>
                        <tbody>
                            @foreach ($alltrainings as $trn)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->user->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->user->position->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">
                                            {{ date('d F Y', strtotime($trn->first_date)) }} -
                                            {{ date('d F Y', strtotime($trn->last_date)) }}</p>
                                    </td>
                                    {{-- @if ($trn->Approval->name == 'rejected') --}}
                                    <td>
                                        @if ($trn->Approval->name == 'rejected')
                                            <span class="badge bg-gradient-danger">
                                                {{ $trn->Approval->name }}
                                            </span>
                                        @elseif($trn->Approval->name == 'accepted')
                                            <span class="badge bg-gradient-success">{{ $trn->Approval->name }}</span>
                                        @elseif($trn->Approval->name == 'submitted')
                                            <span class="badge bg-gradient-warning">{{ $trn->Approval->name }}</span>
                                        @elseif($trn->Approval->name == 'done')
                                            <span class="badge bg-gradient-info">{{ $trn->Approval->name }}</span>
                                        @endif
                                        {{-- <p class="text-xs font-weight-bold mb-0 b">{{ $trn->Approval->name }}</p> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
