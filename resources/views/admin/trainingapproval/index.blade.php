@extends('layouts.main')
@section('content-wrapper')
    <div class="container-fluid py-4">
        @if (session()->has('success'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('success') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/trainingapproval" class="btn btn-primary">Ok!</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (session()->has('rejected'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--danger">
                        <span class="swal-icon--danger__line swal-icon--danger__line--long"></span>
                        <span class="swal-icon--danger__line swal-icon--danger__line--tip"></span>
                        <div class="swal-icon--danger__ring"></div>
                        <div class="swal-icon--danger__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('rejected') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/trainingapproval" class="btn btn-primary">Ok</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (session()->has('addnote'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('addnote') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/trainingapproval" class="btn btn-primary">Close</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12">

                @foreach ($data as $dt)
                    <div class="card my-7 mx-9">
                        <div class="card-header p-0 position-relative mt-n5  mx-7 z-index-2">
                            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                <h4 class="text-white text-capitalize ps-3" align="center"> {{ $dt->company->name }} </h4>
                                <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }}</h4>
                                <h6 class="text-white text-capitalize ps-3" align="center">
                                    {{ date('d F Y', strtotime($dt->first_date)) }} until
                                    {{ date('d F Y', strtotime($dt->last_date)) }}
                                </h6>
                            </div>
                        </div>
                        <div class="card-body px-0 pb-2">

                            <h5 class="text-black text-capitalize ps-3">Officer Name : {{ $dt->user['name'] }}</h5>
                            {{-- {{ $position }}
                            @foreach ($position as $item) --}}
                            <h5 class="text-black text-capitalize ps-3">Positions : {{ $dt->user->position->name }} </h5>
                            {{-- @endforeach --}}

                            <h6 class="text-black text-capitalize ps-3">{{ $dt->note }} </h6>
                            {{-- <h6 class="text-black text-capitalize ps-3">Officer Role : {{ $dt->position['name'] }}</h6> --}}
                            <div class="row" align="right" style="margin-right:2%">
                                <div class="col-12 text-right">

                                    <a href="/trainingapproval/details/{{ $dt->id }}"
                                        class="btn btn-sm btn-warning">Details</a>
                                    {{-- @if (approval_id = 1) --}}
                                    <a class="btn btn-sm btn-danger"
                                        href="/trainingapprovalDenied/ {{ $dt->id }}">Reject</a>

                                    <a class="btn btn-sm btn-success"
                                        href="/trainingapprovalAccept/ {{ $dt->id }}">Accept</a>
                                    <form method="POST"action="/trainingapproval/note/{{ $dt->id }} ">
                                        @method('put')
                                        @csrf
                                        <div class="input-group input-group-outline my-3 mx-3">
                                            <label class="form-label">Note</label>
                                            <input type="text" class="form-control" name="note">
                                        </div>
                                        <input type="submit" class="btn btn-light btn-sm" value="Add Note">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahNote" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Some Reason</h5>
                    {{-- <button type="button" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button> --}}

                </div>
                <div class="modal-body">
                    @foreach ($data as $dt)
                        @csrf
                        <form method='post' action="/trainingapproval/note/{{ $dt->id }}">
                            {{-- {{ $dt }} --}}
                    @endforeach

                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Note</label>
                        <input type="text" class="form-control" name="note">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="submit">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- <script>
    $('.response').click(function() {
        var resposne = $(this).attr('id');
        // It gives you: approved or declined

        // make an ajax call with this response and update the data in database
    });
</script> --}}
