@extends('layouts.main')

@section('content-wrapper')
    <div class="content-wrapper">
        <form method="post" action="/position/{{ $position->id }}" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Position</h4>
                        <!-- <p class="card-description">
                                            Basic form elements
                                          </p> -->

                        <div class="form-group">
                            <label for="name"><b>Name</b></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                placeholder="name" name="name" required autofocus
                                value="{{ old('name', $position->name) }}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-success me-2"
                            onclick="showSwal('success-message')">Update</button>
                        <a href="/position" class="btn btn-light">Cancel</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
