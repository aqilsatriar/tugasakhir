@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-light shadow-success border-radius-lg pt-4 pb-3">
                            <h4 class="text-black text-capitalize ps-3" align="center">Employee Position Of KitaDigi</h4>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="row" align="right" style="margin-right:2%">
                            <div class="col-12 text-right">
                                <a class="btn btn-sm btn-success" href="/company/create">Create New</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-20">
                                        No
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-20 ps-2">
                                        Name
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-20 ps-2">
                                        Action
                                    </th>
                                    <th class="text-secondary opacity-20"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($position as $pos)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $loop->iteration }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">
                                                {{ $pos->name }}</p>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="/position/{{ $pos->id }}/edit" class="material-icons">edit
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#modal-hapus{{ $pos->id }}"
                                                class="material-icons">delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- //Modal Delete --}}
@endsection


@foreach ($position as $pos)
    <div class="modal fade" id="modal-hapus{{ $pos->id }}" tabindex="-1" role="dialog"
        aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Yakin ingin menghapus posisi ini?</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="/position/{{ $pos->id }}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-primary">Hapus</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<!-- End Modal Delete Pengalaman Kegiatan -->
