@extends('layouts.main')

@section('content-wrapper')
    <div class="content-wrapper">
        <form method="post" action="/position" enctype="multipart/form-data">
            @csrf
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Position</h4>
                        <!-- <p class="card-description">
                                Basic form elements
                              </p> -->

                        <div class="form-group">
                            <label for="name"><b>Name</b></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                placeholder="Input positions name..." name="name"
                                @error('name') <div class="invalid-feedback">
                                    {{ $message }}
                                </div> @enderror
                                </div>
                            <button type="submit" class="btn btn-primary me-2"
                                onclick="showSwal('success-message')">Submit</button>
                            <a href="/position" class="btn btn-light">Cancel</a>
                        </div>
                    </div>
                </div>
        </form>

    </div>
@endsection
