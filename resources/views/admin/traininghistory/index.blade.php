@extends('layouts.main')
@section('content-wrapper')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                @foreach ($data as $dt)
                    <div class="card my-5 mx-9">
                        <div class="card-header p-0 position-relative mt-n4  mx-5 z-index-2">
                            {{-- @if ($dt->approval_id == '3')
                                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center"> {{ $dt->company }} </h4>
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }}</h4>
                                </div> --}}
                            @if ($dt->approval_id == '2')
                                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center"> {{ $dt->company }}
                                    </h4>
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->applyname }}
                                    </h4>
                                </div>
                            @endif
                        </div>
                        {{-- <div class="card-header p-0 position-relative mt-n5 mx-3 z-index-2">
                            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                <h4 class="text-white text-capitalize ps-3" align="center"> {{ $dt->company }} </h4>
                                <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }}</h4>
                            </div>
                        </div> --}}
                        <div class="card-body px-0 pb-2">
                            <h6 class="text-black text-capitalize ps-3">Officer Name : {{ $dt->username }}</h6>
                            <h6 class="text-black text-capitalize ps-3">Role : {{ $dt->posname }} </h6>


                            {{-- <h6 class="text-black text-capitalize ps-3">Officer Role : {{ $dt->position['name'] }}</h6> --}}
                            <div class="row" align="right" style="margin-right:2%">
                                <div class="col-12 text-right">
                                    <a href="/traininghistory/details/{{ $dt->trainingId }}"
                                        class="btn btn-sm btn-light">Details</a>
                                    {{-- @if (approval_id = 1) --}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

{{-- <script>
    $('.response').click(function() {
        var resposne = $(this).attr('id');
        // It gives you: approved or declined

        // make an ajax call with this response and update the data in database
    });
</script> --}}
