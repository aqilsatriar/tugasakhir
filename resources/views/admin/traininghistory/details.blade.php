{{-- @extends('layouts.main')
@section('content-wrapper')
    {{-- @foreach ($user as $usr) --}}
{{-- @method('put')
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($applytraining->prove_pic) }}"
                            style="width:440px;height:220px;margin-top:20px;" class="img-fluid" alt=""><br><br>
                    </center>
                    <h5> Training Picture </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="input-group input-group-static my-3">
                <label> Training Name </label>
                <input type="text" name="name" value="{{ $applytraining->name }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Organized by : </label>
                <input type="text" name="company" value="{{ $applytraining->company }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Start Date</label>
                <input type="text" name="first_date" class="form-control" id="date"
                    value="{{ date('d F Y', strtotime($applytraining->first_date)) }}" disabled>
                @error('date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group input-group-static my-3">
                <label> End Date</label>
                <input type="text" name="last_date" class="form-control" id="date"
                    value="{{ date('d F Y', strtotime($applytraining->first_date)) }}" disabled>
                @error('date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group input-group-static my-3">
                <label> Price (in Rupiahs) </label>
                <input type="text" name="price" value="{{ $applytraining->price }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Website Link (If Online) </label>
                <input type="url" name="link" value="{{ $applytraining->link }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Training Description </label>
                <input type="text" name="deskripsi" value="{{ $applytraining->deskripsi }}" class="form-control"
                    disabled>
            </div>
        </div>
    </div> --}}


{{-- @endforeach --}}
{{-- <a href="/traininghistory"><button type="button" class="btn btn-secondary">Close</button></a>
@endsection --}}


@extends('layouts.main')
@section('content-wrapper')
    {{-- @foreach ($user as $usr) --}}
    @method('put')

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($applytraining->prove_pic) }}"
                            style="width:440px;height:220px;margin-top:20px;" class="img-fluid" alt=""><br><br>
                    </center>
                    <h5> Training Picture </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="input-group input-group-static my-3">
                <label> Training Name </label>
                <input type="text" name="name" value="{{ $applytraining->name }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Organized by : </label>
                <input type="text" name="company" value="{{ $applytraining->company->name }}" class="form-control"
                    disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Start Date</label>
                <input type="text" name="first_date" class="form-control" id="date"
                    value="{{ date('d F Y', strtotime($applytraining->first_date)) }}" disabled>
                @error('date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group input-group-static my-3">
                <label> End Date</label>
                <input type="text" name="last_date" class="form-control" id="date"
                    value="{{ date('d F Y', strtotime($applytraining->last_date)) }}" disabled>
                @error('date')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group input-group-static my-3">
                <label> Price (in Rupiahs) </label>
                <input type="text" name="price" value="{{ $applytraining->price }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Website Link (If Online) </label>
                <input type="url" name="link" value="{{ $applytraining->link }}" class="form-control" disabled>
            </div>
            <div class="input-group input-group-static my-3">
                <label> Training Description </label>
                <input type="text" name="deskripsi" value="{{ $applytraining->deskripsi }}" class="form-control"
                    disabled>
            </div>
        </div>
    </div>

    {{-- @endforeach --}}
    <a href="/traininghistory"><button type="button" class="btn btn-secondary">Close</button></a>
@endsection
