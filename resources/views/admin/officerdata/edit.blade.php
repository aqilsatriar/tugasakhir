@extends('layouts.main')
@section('content-wrapper')
    {{-- @foreach ($user as $usr) --}}
    <div class="row">
        @if (session()->has('success'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('success') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/officerdata" class="btn btn-primary">Close</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($user->picture) }}" style="width:200px;height:220px;margin-top:20px;"
                            class="img-fluid" alt=""><br>
                        <h5> Current Picture </h5>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($user->identity_picture) }}"
                            style="width:300px;height:220px;margin-top:20px;" class="img-fluid" alt=""><br>
                        <h5> Current Identity Picture </h5>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-5">
        <div class="card-body">
            <form action="/officerdata/edit/{{ $user->id }}" method='post' enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="input-group input-group-static my-3">
                    <label>Name</label>
                    <input type="text" name="name" required autofocus value="{{ old('name', $user->name) }}"
                        class="form-control">
                </div>
                <div class="input-group input-group-static my-3">
                    <label>E-mail</label>
                    <input type="email" name="email" required autofocus value="{{ old('email', $user->email) }}"
                        class="form-control">
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Password</label>
                    <input type="password" name="password" required autofocus value="{{ old('password', $user->password) }}"
                        class="form-control">
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Address</label>
                    <input type="text" name="address" required autofocus value="{{ old('address', $user->address) }}"
                        class="form-control">
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Place Date</label>
                    <input type="text" name="place_date" required autofocus
                        value="{{ old('place_date', $user->place_date) }}" class="form-control">
                </div>

                <div class="input-group input-group-static my-3">
                    <label>Birth Date</label>
                    <input type="date" name="birth_date" class="form-control @error('date') is-invalid @enderror "
                        id="date" name="birth_date" value="{{ old('birth_date', $user->birth_date) }}">
                    @error('date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Join Date</label>
                    <input type="date" name="join_date" class="form-control @error('date') is-invalid @enderror "
                        id="date" name="join_date" value="{{ old('join_date', $user->join_date) }}">
                    @error('date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Roles</label>
                    <select class="form-control" aria-label="Default select example" name="roles_id" <br>
                        {{-- <option selected> -- Pilih Role -- </option> --}}
                        <option value="{{ $user->roles->roles }}" selected>{{ $user->roles->roles }}</option>
                        <option value="1">
                            Admin
                        <option value="2">
                            Officer
                        </option>
                    </select>
                </div>
                <div class="input-group input-group-static my-3">
                    <label>Position</label>
                    <select class="form-control" aria-label="Default select example"
                        name="positions_id"value="{{ old('positions_id', $user->positions_id) }}"><br>
                        {{-- <option selected> -- Pilih Jabatan --</option> --}}
                        @foreach ($tampilPosition as $data)
                            @if (old('positions_id') == $data->id)
                                <option value="{{ $data->id }}" selected>
                                    {{ $data->name }}
                                </option>
                            @else
                                <option value="{{ $data->id }}">
                                    {{ $data->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="mb-1">
                    <label class="col-sm-3 col-form-label"> Picture</label>
                    <img class="img-preview img-fluid mb-3 col-sm-5">
                    <input class="form-control @error('image') is-invalid @enderror" type="file" id="picture"
                        name="picture" onchange="previewImage()">
                    @error('image')
                        <div class="invalid-feedback">
                            {{ @message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-1">
                    <label class="col-sm-3 col-form-label">Identity Picture</label>
                    <img class="img-preview img-fluid mb-3 col-sm-5">
                    <input class="form-control @error('image') is-invalid @enderror" type="file" id="identitypic"
                        name="identity_picture" onchange="previewIdentity()">
                    @error('image')
                        <div class="invalid-feedback">
                            {{ @message }}
                        </div>
                    @enderror
                </div>

                {{-- @endforeach --}}
                <input type="submit" class="btn btn-primary" value="Save">
                <a href="/officerdata"><button type="button" class="btn btn-secondary">Cancel</button></a>
                @csrf
            </form>
        </div>
    </div>
@endsection

{{-- <script>
    function previewImage() {
        const picture = document.querySelector('#picture');
        const imgPreview = document.querySelector('.img-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(picture.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }

    function previewIdentity() {
        const indentitypic = document.querySelector('#identitypic');
        const imgPreview = document.querySelector('.img-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(indentitypic.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }
</script> --}}

<script>
    function previewImage() {
        const picture = document.querySelector('#picture');
        const imgPreview = document.querySelector('.pic-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(picture.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }

    function previewIdentity() {
        const indentitypic = document.querySelector('#identitypic');
        const imgPreview = document.querySelector('.idn-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(indentitypic.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
