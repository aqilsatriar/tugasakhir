<!DOCTYPE html>
<html>

<head>
    <title>Officer Data Download</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    <h4>
        <center>Officer Data of KitaDigi <BR>
    </h4>
    <center>
        <p>Griya Edelweis blok F10, Dusun V, Joho,
            Kec. Mojolaban, Kabupaten Sukoharjo, Jawa Tengah 57554.
            E-mail : cs@kitadigi.com Website : www.kitadigi.com
        </p>
    </center>
    <hr style="width:max;height:2px;">
    <b>
    </b>
    <br>

    <table class='table table-bordered'>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Position</th>
                <th>Birth Date</th>
                <th>Join Date</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            @php $i=1 @endphp
            @foreach ($user as $usr)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $usr->username }}</td>
                    <td>{{ $usr->email }}</td>
                    <td>{{ $usr->posname }}</td>
                    <td>{{ date('d F Y', strtotime($usr->birth_date)) }}</td>
                    </h5>
                    <td>{{ date('d F Y', strtotime($usr->join_date)) }}</td>
                    <td>{{ $usr->address }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>
