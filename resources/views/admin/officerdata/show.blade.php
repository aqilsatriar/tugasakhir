@extends('layouts.main')
@section('content-wrapper')
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($user->picture) }}" style="width:200px;height:220px;margin-top:20px;"
                            class="img-fluid" alt=""><br>
                        <h5> Picture </h5>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <center><img src="{{ asset($user->identity_picture) }}"
                            style="width:300px;height:220px;margin-top:20px;" class="img-fluid" alt=""><br>
                        <h5> Identity Picture </h5>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div class="input-group input-group-static my-3">
        <label> Officer Name </label>
        <input readonly type="text" name="name" required autofocus value="{{ old('name', $user->name) }}"
            class="form-control">
    </div>
    <div class="input-group input-group-static my-3">
        <label> Officer E-Mail</label>
        <input readonly type="email" name="email" required autofocus value="{{ $user->email }}" class="form-control">
    </div>
    <div class="input-group input-group-static my-3">
        <label> Officer Address </label>
        <input type="text" name="address" required autofocus value="{{ $user->address }}" class="form-control" readonly>
    </div>
    <div class="input-group input-group-static my-3">
        <label>Officer Place Date</label>
        <input type="text" name="place_date" required autofocus value="{{ $user->place_date }}" class="form-control"
            readonly>
    </div>

    <div class="input-group input-group-static my-3">
        <label><b>Officer Birth Date</b></label>
        <input readonly type="text" name="birth_date" class="form-control" id="birth_date"
            value=" {{ date('d F Y', strtotime($user->birth_date)) }}">
    </div>
    <div class="input-group input-group-static my-3">
        <label><b>Officer Join Date</b></label>
        <input readonly type="text" name="join_date" class="form-control" id="join_date"
            value=" {{ date('d F Y', strtotime($user->join_date)) }}">
    </div>

    <div class="input-group input-group-static my-3">
        <label> Officer Role</label>
        <input readonly type="text" name="roles_id" class="form-control" id="roles_id" name="roles_id"
            value="{{ $user->roles->roles }}">
    </div>

    <div class="input-group input-group-static my-3">
        <label> Officer Position</label>
        <input readonly type="text" name="positions_id" class="form-control" id="positions_id" name="positions_id"
            value="{{ $user->position->name }}">
    </div>

    <a href="/officerdata"><button type="button" class="btn btn-secondary">Close</button></a>
@endsection
