@extends('layouts.main')
@section('content-wrapper')
    <div class="container-fluid py-4">
        @if (session()->has('success'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('success') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/officerdata" class="btn btn-primary">Ok!</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-5 z-index-2">
                        <div class="bg-gradient-primary shadow-light border-radius-lg pt-3 pb-3">
                            <h3 class="text-white text-capitalize ps-3" align="center">Employee Of KitaDigi</h3>
                            <p class="text-white text-capitalize ps-3" align="center"> All officer ready to sail, captain!
                            </p>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-0 pt-3">
                        <div class="row" align="right" style="margin-right:2%">
                            <div class="col-12 text-right">
                                <a href="/officerdata/download_pdf" class="btn btn-sm btn-light"
                                    target="_blank">Download</a>
                                <a href="/officerdata/print_pdf" class="btn btn-sm btn-light">Print</a>
                                <a class="btn btn-sm btn-success" href="#" data-bs-toggle="modal"
                                    data-bs-target="#tambahUser">Create New</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive pt-1">
                        <table id="TableOfc" class="table align-items-center mb-0">
                            <thead class="text-warning">
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Id</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Name</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Email</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Position</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Join Date</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Role</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder">
                                    Action</th>

                            </thead>
                            <tbody>
                                @foreach ($user as $usr)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $usr->usId }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $usr->username }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $usr->email }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $usr->posname }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">
                                                {{ date('d F Y', strtotime($usr->join_date)) }}
                                            </p>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <p class="text-xs font-weight-bold mb-0">{{ $usr->roles }}</p>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="/officerdata/edit/{{ $usr->usId }}" class="material-icons">edit
                                            </a>
                                            <a href="/officerdata/show/{{ $usr->usId }}" class="material-icons">details
                                            </a>
                                            <a data-bs-toggle="modal" data-bs-target="#modal-hapus{{ $usr->usId }}"
                                                class="material-icons">delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Create -->
    <div class="modal fade" id="tambahUser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Officer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    @csrf
                    <form method='post' action="createofficerdata" enctype="multipart/form-data">
                        {{-- <div class="row pt-6">
                            <div class="col-6">
                                <div class="card" data-animation="true">
                                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                                        <a class="d-block blur-shadow-image">
                                            <img src="" class="img-fluid shadow border-radius-lg">
                                        </a>
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="d-flex mt-n6 mx-auto">
                                            <a class="btn btn-link text-primary ms-auto border-0" data-bs-toggle="tooltip"
                                                data-bs-placement="bottom" title="Delete">
                                                <i class="material-icons text-lg">delete</i>
                                            </a>
                                            <button class="btn btn-link text-info me-auto border-0" data-bs-toggle="tooltip"
                                                data-bs-placement="bottom" title="Add">
                                                <i class="material-icons text-lg">add_box</i>
                                            </button>
                                        </div>
                                        <h5 class="font-weight-normal mt-3">
                                            <a href="javascript:;"> Picture</a>
                                        </h5>
                                        <p class="mb-0">
                                            Add Officer Picture
                                        </p>
                                    </div>
                                    <hr class="dark horizontal my-0">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card" data-animation="true">
                                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                                        <a class="d-block blur-shadow-image">
                                            <img src="https://demos.creative-tim.com/test/material-dashboard-pro/assets/img/products/product-1-min.jpg"
                                                alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
                                        </a>
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="d-flex mt-n6 mx-auto">
                                            <a class="btn btn-link text-primary ms-auto border-0" data-bs-toggle="tooltip"
                                                data-bs-placement="bottom" title="Delete">
                                                <i class="material-icons text-lg">delete</i>
                                            </a>
                                            <button class="btn btn-link text-info me-auto border-0"
                                                data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add">
                                                <i class="material-icons text-lg">add_box</i>
                                            </button>
                                        </div>
                                        <h5 class="font-weight-normal mt-3">
                                            <a href="javascript:;"> Identity Picture</a>
                                        </h5>
                                        <p class="mb-0">
                                            Add Officer Identity Picture
                                        </p>
                                    </div>
                                    <hr class="dark horizontal my-0">
                                </div>
                            </div>
                        </div> --}}
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Email</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Address</label>
                            <input type="text" name="address" class="form-control">
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Place Date</label>
                            <input type="text" name="place_date" class="form-control">
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="date" class="col-sm-3 col-form-label">Birth
                                Date</label>
                            <div class="col-sm-3">
                                <input type="date" name="birth_date"
                                    class="form-control @error('date') is-invalid @enderror " id="date"
                                    name="birth_date" value="{{ old('date') }}">
                                @error('date')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="date" class="col-sm-3 col-form-label">Join Date
                            </label>
                            <div class="col-sm-3">
                                <input type="date" name="join_date"
                                    class="form-control @error('date') is-invalid @enderror " id="date"
                                    name="join_date" value="{{ old('date') }}">
                                @error('date')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Role</label>
                            <br>
                            <select class="form-select" aria-label="Default select example" name="roles_id">
                                <option selected> Pilih Role </option>
                                <option value="1">
                                    Admin
                                <option value="2">
                                    Employee
                                </option>
                            </select>
                        </div>
                        <div class="mb-1 mx-4">
                            <label for="" class="col-sm-3 col-form-label">Position</label>
                            <br>
                            <select class="form-select" aria-label="Default select example" name="positions_id">
                                <option selected> Pilih Jabatan </option>
                                @foreach ($tampilPosition as $data)
                                    @if (old('positions_id') == $data->id)
                                        <option value="{{ $data->id }}" selected>
                                            {{ $data->name }}
                                        </option>
                                    @else
                                        <option value="{{ $data->id }}">
                                            {{ $data->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-1 mx-4">
                            <label class="col-sm-3 col-form-label">Picture</label>
                            <img class="pic-preview img-fluid mb-3 col-sm-5">
                            <input class="form-control @error('picture') is-invalid @enderror" type="file"
                                id="picture" name="picture" onchange="previewImage()">
                            @error('picture')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="mb-1 mx-4">
                            <label class="col-sm-3 col-form-label">Identity Picture</label>
                            <img class="idn-preview img-fluid mb-3 col-sm-5">
                            <img class="img-preview img-fluid">
                            <input class="form-control @error('identity_picture') is-invalid @enderror" type="file"
                                id="identitypic" name="identity_picture" onchange="previewIdentity()">
                            @error('identity_picture')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="submit">

                    <button type="button" class="btn btn-secondary" href="/officerdata"
                        data-bs-dismiss="modal">Close</button>
                    @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- //Modal Delete --}}
    @foreach ($user as $usr)
        <div class="modal fade" id="modal-hapus{{ $usr->usId }}" tabindex="-1" role="dialog"
            aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Yakin Ingin Menghapus?</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="/officerdata/{{ $usr->usId }}" method="post">
                                    {{-- @method('delete') --}}
                                    @csrf
                                    <button class="btn btn-primary">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script
        src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.jshttps://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js">
    </script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    {{-- <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('#TableOfc').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endpush


<script>
    function previewImage() {
        const picture = document.querySelector('#picture');
        const imgPreview = document.querySelector('.pic-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(picture.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }

    function previewIdentity() {
        const indentitypic = document.querySelector('#identitypic');
        const imgPreview = document.querySelector('.idn-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(indentitypic.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>



{{-- <script>
    function ProfileContainer = () => {
        const [KTPImageUrl, setKTPImageUrl] = useState(null);
        const [photoWithKTPImageUrl, setPhotoWithKTPImageUrl] = useState(null);
        const inputKTPImage = useRef(null);
        const inputPhotoWithKTPImage = useRef(null);

        const submitProfile = () => {
            console.log('tes');
            setLocalStorageValue('profile', {
                profile: 'profile'
            });
            router.push('/');
        };
    }

    function onUploadButtonClick = (e, type) => {
        if (type == 'KTP') {
            inputKTPImage.current.click();
        } else {
            inputPhotoWithKTPImage.current.click();
        }
        console.log(inputKTPImage);
        console.log(inputPhotoWithKTPImage);
        console.log(type);
    };

    function inputImage = (e, type) => {
        const files =
            type == 'KTP' ?
            inputKTPImage.current?.files :
            inputPhotoWithKTPImage.current?.files;
        var reader = new FileReader();
        if (type == 'KTP') {
            reader.onload = function(e) {
                setKTPImageUrl(e.target.result);
            };
        } else {
            reader.onload = function(e) {
                setPhotoWithKTPImageUrl(e.target.result);
            };
        }
        reader.readAsDataURL(files[0]);
    };
</script> --}}
