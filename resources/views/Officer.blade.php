@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">warning</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Submitted</p>
                            <h1 class="mb-0">{{ $apvTrainings }}</h1>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">list</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Training</p>
                            <h1 class="mb-0"> {{ $totalTrainings }} </h1>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header p-0 position-relative mt-n4 mt-3 mx-3 z-index-2">
                    <div class="bg-gradient-warning shadow-light border-radius-lg pt-4 pb-3">
                        <h3 class="text-black text-capitalize ps-3" align="center">Training Recapitulation
                        </h3>
                        <h5 class="text-white text-capitalize ps-3" align="center">"There is no elevator to success. You
                            have
                            to take the stairs."
                        </h5>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table id="OfcTrainingTable" class="table table-hover">
                        <thead class="text-warning">
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                No</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Training Name</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Company</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Date</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Location</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Price</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Status</th>
                        </thead>
                        <tbody>
                            @foreach ($trainingsofc as $trn)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->company->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">
                                            {{ date('d F Y', strtotime($trn->first_date)) }} -
                                            {{ date('d F Y', strtotime($trn->last_date)) }}</p>
                                    </td>

                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->location }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $trn->price }}</p>
                                    </td>
                                    <td>
                                        @if ($trn->Approval->name == 'rejected')
                                            <span class="badge bg-gradient-danger">
                                                {{ $trn->Approval->name }}
                                            </span>
                                        @elseif($trn->Approval->name == 'accepted')
                                            <span class="badge bg-gradient-success">{{ $trn->Approval->name }}</span>
                                        @elseif($trn->Approval->name == 'submitted')
                                            <span class="badge bg-gradient-warning">{{ $trn->Approval->name }}</span>
                                        @elseif($trn->Approval->name == 'done')
                                            <span class="badge bg-gradient-info">{{ $trn->Approval->name }}</span>
                                        @endif
                                </tr>
                            @endforeach
                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    {{-- <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('#OfcTrainingTable').DataTable();
        });
    </script>
@endpush
