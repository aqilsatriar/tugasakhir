@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid
        py-4">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-warning shadow-warning text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">warning</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Submission</p>
                            <h1 class="mb-0">{{ $trainings }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/training/trainingsubmission" class="btn btn-sm btn-warning">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">checklist</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Accepted</p>
                            <h1 class="mb-0"> {{ $trainingsAcc }} </h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/training/trainingacc" class="btn btn-sm btn-success">Details</a>
                            </div>
                        </div>

                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">checklist</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Done</p>
                            <h1 class="mb-0"> {{ $trainingsDone }} </h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/training/trainingdone" class="btn btn-sm btn-info">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">close</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Rejected</p>
                            <h1 class="mb-0">{{ $trainingsReject }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/training/trainingreject" class="btn btn-sm btn-primary">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-warning shadow-warning text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">people</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Officer</p>
                            <h1 class="mb-0">{{ $officers }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dataofficer/officer" class="btn btn-sm btn-warning">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">support_agent</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Admin</p>
                            <h1 class="mb-0"> {{ $admin }} </h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dataofficer/admin" class="btn btn-sm btn-success">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">face</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Total Employee</p>
                            <h1 class="mb-0"> {{ $employee }} </h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dataofficer/employee" class="btn btn-sm btn-info">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
        </div>
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script> --}}
    @endsection
