@extends('layouts.main')

@section('content-wrapper')
    <div class="container">
        <div class="card mt-3 mb-3">
            <div class="card-header p-0 position-relative mt-n4 mt-3 mx-3 z-index-2">
                <div class="bg-gradient-warning shadow-warning border-radius-lg pt-4 pb-3">
                    <h3 class="text-black text-capitalize ps-3" align="center">Officer
                    </h3>
                    <p class="text-white text-capitalize ps-3" align="center">All Officer in
                        Kita Monster Digital
                    </p>
                </div>
            </div>
            <div class="card-body px-0 pb-0 pt-3">

                <div class="card-body table-responsive pt-1">
                    <table id="myTable" class="table table-hover">
                        <thead class="text-warning">
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                No</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Name</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Role</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Email</th>
                            <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                Join Date</th>
                        </thead>
                        <tbody>
                            @foreach ($allofficer as $ofc)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $ofc->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $ofc->position->name }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $ofc->email }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">
                                            {{ date('d F Y', strtotime($ofc->join_date)) }}
                                    </td>
                                    {{-- @if ($trn->Approval->name == 'rejected') --}}
                                </tr>
                            @endforeach
                        </tbody>


                    </table>
                </div>
            </div>
        </div>
        <a href="/redirects"><button type="button" class="btn btn-secondary ">Close</button></a>
    </div>
@endsection
