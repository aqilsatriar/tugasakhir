@extends('layouts.main')

@section('content-wrapper')
    <div class="row">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <h1 class="text-primary">Welcome!</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
