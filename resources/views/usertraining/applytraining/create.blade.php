@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid py-4">
        @if (session()->has('success'))
            <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true">
                    <div class="swal-icon swal-icon--success">
                        <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                        <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                        <div class="swal-icon--success__ring"></div>
                        <div class="swal-icon--success__hide-corners"></div>
                    </div>
                    <div class="swal-title" style="">{{ session('success') }}</div>
                    <div class="swal-footer">
                        <div class="swal-button-container">
                            <a href="/applytraining/create" class="btn btn-primary">Ok!</a>
                            <div class="swal-button__loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-body px-4 pb-2">
                        <form method="post" action="/createTraining" enctype="multipart/form-data">
                            @csrf
                            <h2> "Don’t let the noise of others opinions drown out your own inner voice."
                                - Steve Jobs </h2>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="name">Training Name &ensp;&ensp;&ensp;</label>
                                </div>

                                <input type="text" name="name"
                                    class="form-control @error('name') is-invalid @enderror" id="name">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="company">Company &ensp;&ensp;&ensp;</label>
                                </div>
                                <select class="form-select" aria-label="Default select example" name="company_id">
                                    <option selected> Choose Company </option>
                                    @foreach ($tampilCompany as $data)
                                        @if (old('positions_id') == $data->id)
                                            <option value="{{ $data->id }}" selected>
                                                {{ $data->name }}
                                            </option>
                                        @else
                                            <option value="{{ $data->id }}">
                                                {{ $data->name }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>


                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="deskripsi">Deskripsi&ensp;</label>
                                </div>
                                <input type="text" name="deskripsi"
                                    class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi">
                                @error('deskripsi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="first_date">First date&ensp;</label>
                                </div>
                                <input type="date" name="first_date"
                                    class="form-control @error('first_date') is-invalid @enderror" id="first_date">
                                @error('first_date')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="last_date">Last date&ensp;</label>
                                </div>
                                <input type="date" name="last_date"
                                    class="form-control @error('last_date') is-invalid @enderror" id="last_date">
                                @error('last_date')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="prove_pic">Prove pic &ensp;&nbsp;</label>
                                </div>
                                <img class="img-preview img-fluid">
                                <input type="file" class="form-control @error('image') is-invalid @enderror"
                                    id="image" name="prove_pic" onchange="previewImage()">
                                @error('image')
                                    <div class="invalid-feedback">
                                        {{ @message }}
                                    </div>
                                @enderror
                            </div>


                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="price">Price&ensp;&ensp;&ensp;&ensp;</label>
                                </div>
                                <input type="number" name="price"
                                    class="form-control @error('price') is-invalid @enderror" id="price">
                                @error('price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="location">Location&ensp;</label>
                                </div>
                                <input type="text" name="location"
                                    class="form-control @error('location') is-invalid @enderror" id="location">
                                @error('location')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline my-3">
                                <div class="col-2">
                                    <label for="link">Link&ensp;&ensp;&ensp;&ensp;&ensp;</label>
                                </div>
                                <input type="url" name="link"
                                    class="form-control @error('link') is-invalid @enderror" id="link">
                                @error('link')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <label for="approval">&ensp;</label>
                            <input type="hidden" name="approval_id"
                                class="form-control @error('approval_id') is-invalid @enderror" id="approval_id"
                                value="1" autocomplete="approval_id">
                            @error('approval_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <button type="submit" class="btn bg-gradient-primary mb-2 mt-5" style="float: right">Submit
                            </button>
                            <br><br>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>

    @if (session()->has('successcompany'))
        <div class="swal-overlay swal-overlay--show-modal" tabindex="-1">
            <div class="swal-modal" role="dialog" aria-modal="true">
                <div class="swal-icon swal-icon--success">
                    <span class="swal-icon--success__line swal-icon--success__line--long"></span>
                    <span class="swal-icon--success__line swal-icon--success__line--tip"></span>
                    <div class="swal-icon--success__ring"></div>
                    <div class="swal-icon--success__hide-corners"></div>
                </div>
                <div class="swal-title" style="">{{ session('successcompany') }}</div>
                <div class="swal-footer">
                    <div class="swal-button-container">
                        <a href="/applytraining/create" class="btn btn-primary">Ok!</a>
                        <div class="swal-button__loader">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{-- <div class="card">

        <div class="input-group input-group-outline my-3">
            <div class="col-2">
                <label for="company"> *If there is no company, create the new one</label>
            </div>
            <div>
                <form method="POST"action="/applytraining/create">
                    @method('post')
                    @csrf
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Add new..</label>
                        <input type="text" class="form-control" name="n">
                        <input type="submit" class="btn btn-light btn-sm" value="Add New Company">
                    </div>
                </form>
            </div>
        </div>
    </div> --}}

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function previewImage() {
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection
