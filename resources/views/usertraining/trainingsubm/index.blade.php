@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                @foreach ($data as $dt)
                    <div class="card my-5 mx-10">

                        <div class="card-header p-0 position-relative mt-n4  mx-7 z-index-2">
                            @if ($dt->name == 'rejected')
                                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @elseif($dt->name == 'accepted')
                                <div class="bg-gradient-success shadow-light border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @elseif($dt->name == 'submitted')
                                <div class="bg-gradient-warning shadow-light border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @endif
                        </div>
                        <div class="card-body px-0 pb-2">
                            <div class="table-responsive p-0">
                                <h4 class="text-black text-capitalize ps-3" align="center">{{ $dt->company }} </h4>
                                <h4 class="text-black text-capitalize ps-3">{{ $dt->applyname }} </h4>
                                <h4 class="text-black text-capitalize ps-3">{{ $dt->location }} </h4>
                                <h5 class="text-black text-capitalize ps-3">
                                    {{ date('d F Y', strtotime($dt->first_date)) }} until
                                    {{ date('d F Y', strtotime($dt->last_date)) }}
                                </h5>
                                <h4 class="text-black text-capitalize ps-3">{{ $dt->note }} </h4>
                                <div class="row" align="right" style="margin-right:2%">
                                    <div class="col-12 text-right">
                                        <a href="/trainingsubm/show/{{ $dt->id }}"
                                            class="btn btn-sm btn-warning">Details</a>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

{{-- modal details --}}
{{-- <div class="modal fade" id="modaldetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-normal"> Details Training</h5>
                <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span id="applyname"> </span>
                <span id="company"> </span>
                <span id="firstdate"> </span>
                <span id="price"> </span>
                <span id="link"> </span>
                <span id="deskripsi"> </span>
                {{-- <h3> {{ $dt->applyname }}</h3>
                <h6> By : {{ $dt->company }}</h6>
                <br>
                <h6> {{ $dt->first_date }} until {{ $dt->last_date }} </h6>
                <h6> Rp. {{ $dt->price }}</h6>
                <h6> {{ $dt->link }}</h6>
                <br>
                <h7> {{ $dt->deskripsi }}</h7> --}}

{{-- </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> --}}

{{-- <script>
    $(document).ready(function() {
        $(document).on('click', '#set_dtl', function() {
            var name = $(this).dt('name');
            $('#name').text(name);
            $('#modal-item').modal('hide');
        })
    })
</script> --}}
