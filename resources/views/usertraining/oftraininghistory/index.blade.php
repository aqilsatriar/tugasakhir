@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @foreach ($data as $dt)
                    <div class="card mb-6 mx-7">
                        <div class="card-header p-0 position-relative mt-n3  mx-5 z-index-2">
                            @if ($dt->name == 'rejected')
                                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @elseif($dt->name == 'accepted')
                                <div class="bg-gradient-success shadow-light border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @elseif($dt->name == 'done')
                                <div class="bg-gradient-info shadow-light border-radius-lg pt-4 pb-3">
                                    <h4 class="text-white text-capitalize ps-3" align="center">{{ $dt->name }} </h4>
                                </div>
                            @endif
                        </div>
                        @if ($dt->name == 'rejected')
                            <div class="card-body px-0 pb-2">
                                <div class="table-responsive p-0">
                                    <h4 class="text-black text-capitalize ps-3" align="center">{{ $dt->company }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->applyname }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->location }} </h4>
                                    <h5 class="text-black text-capitalize ps-3">
                                        {{ date('d F Y', strtotime($dt->first_date)) }} until
                                        {{ date('d F Y', strtotime($dt->last_date)) }}
                                    </h5>
                                    <h4 class="text-black text-capitalize ps-3">Note : {{ $dt->note }} </h4>

                                </div>
                            </div>
                        @elseif($dt->name == 'accepted')
                            <div class="card-body px-0 pb-2">
                                <div class="table-responsive p-0">
                                    <h4 class="text-black text-capitalize ps-3" align="center">{{ $dt->company }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->applyname }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->location }} </h4>
                                    <h5 class="text-black text-capitalize ps-3">
                                        {{ date('d F Y', strtotime($dt->first_date)) }} until
                                        {{ date('d F Y', strtotime($dt->last_date)) }}
                                    </h5>
                                    <h4 class="text-black text-capitalize ps-3">Note : {{ $dt->note }} </h4>
                                    <h6 class="text-black text-capitalize ps-3">*Reimbursement will be appear if you
                                        finished the training and upload the certificate into this system </h6>

                                    <div class="row mt-3" align="right" style="margin-right:2%">
                                        <div class="col-12 text-right">
                                            <a class="btn btn-sm btn-success" href="#" data-bs-toggle="modal"
                                                data-bs-target="#tambahSertif-{{ $dt->id }}">Upload Certificate</a>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        @elseif($dt->name == 'done')
                            <div class="card-body px-0 pb-2">
                                <div class="table-responsive p-0">
                                    <h4 class="text-black text-capitalize ps-3" align="center">{{ $dt->company }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->applyname }} </h4>
                                    <h4 class="text-black text-capitalize ps-3">{{ $dt->location }} </h4>
                                    <h5 class="text-black text-capitalize ps-3">
                                        {{ date('d F Y', strtotime($dt->first_date)) }} until
                                        {{ date('d F Y', strtotime($dt->last_date)) }}
                                    </h5>
                                    <h4 class="text-black text-capitalize ps-3">Note : {{ $dt->note }} </h4>


                                    <div class="row mt-3" align="right" style="margin-right:2%">
                                        <div class="col-12 text-right">

                                            <a href="/oftraininghistory/print/{{ $dt->id }}"
                                                class="btn btn-sm btn-info"> Print Reimbursement</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @foreach ($data as $upl)
        <div class="modal fade" id="tambahSertif-{{ $upl->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Certificate</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                    </div>

                    <div class="modal-body">
                        <form method="POST" action="updateCertificate/{{ $upl->id }}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="mb-1 mx-4">
                                <label class="col-sm-3 col-form-label">Certificate</label>
                                <img class="pic-preview img-fluid mb-3 col-sm-5">
                                <input class="form-control @error('picture') is-invalid @enderror" type="file"
                                    id="picture" name="certificate" onchange="previewImage()">
                                @error('picture')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" value="submit">
                        <button type="button" class="btn btn-secondary" href="/traininghistory"
                            data-bs-dismiss="modal">Close</button>
                        @csrf
                        </form>
                    </div>

                </div>
            </div>
        </div>
    @endforeach
    <script>
        function previewImage() {
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection
