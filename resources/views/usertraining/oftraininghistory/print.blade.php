<!DOCTYPE html>
<html>

<head>
    <title>Training Reimburse</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<div>
    <h3>
        <center>Triple One Global Global Group <BR> PT. KITA MONSTER DIGITAL
    </h3>
    <center style="margin-left:150px;margin-right:150px;margin-top:20px;">
        <p>Griya Edelweis blok F10, Dusun V, Joho,
            Kec. Mojolaban, Kabupaten Sukoharjo, Jawa Tengah 57554 <br> Fax : 021-2256-2887, Phone : 081-6996-830 ,
            Email : cs@kitadigi.com , Website : kitadigi.com</p>
    </center>
    <hr style="width:max;height:2px;">
    <h4><b>
            <center> TRAINING REIMBURSEMENT</center>
        </b></h4><br>

    <body>
        @foreach ($data as $dt)
            <table style="margin-left:150px;margin-top:20px;">
                <tr>
                    <td style="width:200px;"> Officer Name</td>
                    <td>:</td>
                    <td><b>{!! $dt->username !!}</b></td>
                </tr>
                <tr>
                    <td>Officer Email</td>
                    <td>:</td>
                    <td><b>{!! $dt->email !!}</b></td>
                </tr>
                <tr>
                    <td>Officer Position</td>
                    <td>:</td>
                    <td><b>{!! $dt->posname !!}</b></td>
                </tr>
            </table>
            <hr>
            <hr>

            <table style="margin-left:150px;margin-top:20px;">

                <tr>
                    <td style="width:200px;">Training Name</td>
                    <td>:</td>
                    <td><b>{!! $dt->applyname !!}</b></td>
                </tr>
                <tr>
                    <td>Company</td>
                    <td>:</td>
                    <td><b>{!! $dt->company !!}</b></td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td>:</td>
                    <td><b>{!! $dt->location !!}</b></td>
                </tr>
                <tr>
                    <td>Training Fee</td>
                    <td>:</td>
                    <td><b> Rp. {!! $dt->price !!}</b></td>
                </tr>
                <tr>
                    <td>Training Start Date</td>
                    <td>:</td>
                    <td><b>{!! date('d F Y', strtotime($dt->first_date)) !!}</b></td>
                </tr>
                <tr>
                    <td>Training End Date</td>
                    <td>:</td>
                    <td><b>{!! date('d F Y', strtotime($dt->last_date)) !!}</b></td>
                </tr>

            </table>
            <p style="margin-left:150px;margin-top:20px;">Note : Please bring proof that you have done training like a
                certificate or something else. If you
                don't attach it then reimbursement can't be done
            </p>
            <br>

            <table style="float:right;margin-right:125px;margin-top:20px;">
                <tr>
                    <td>Surakarta, {!! date('d F Y') !!}</td>
                </tr>
                <tr>
                    <td>Human Resource<br>
                        <img src="/img/baqis1-ttd.png" style="width:180px;height:120px;margin-top:20px;"
                            class="img-fluid">
                    </td>
                </tr>
                <tr>
                    <td><b> Baqiyatus Shaliha </b></td>

                </tr>
            </table>
        @endforeach
</div>

</body>

</html>
<script type="text/javascript">
    window.print();
</script>
