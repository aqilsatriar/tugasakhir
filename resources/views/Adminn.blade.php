@extends('layouts.main')

@section('content-wrapper')
    <div class="container-fluid
        py-4">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-warning shadow-warning text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">warning</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Submission</p>
                            <h1 class="mb-0">{{ $trainings }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dashtraining/trainingsubmission" class="btn btn-sm btn-warning">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">close</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Rejected</p>
                            <h1 class="mb-0">{{ $trainingsReject }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dashtraining/trainingreject" class="btn btn-sm btn-danger">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">checklist</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Accepted</p>
                            <h1 class="mb-0"> {{ $trainingsAcc }} </h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dashtraining/trainingacc" class="btn btn-sm btn-success">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card">
                    <div class="card-header p-3 pt-2">
                        <div
                            class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                            <i class="material-icons opacity-10">check</i>
                        </div>
                        <div class="text-end pt-1">
                            <p class="text-sm mb-0 text-capitalize">Done</p>
                            <h1 class="mb-0">{{ $trainingsDone }}</h1>
                            <div class="col-14 mt-3  text-right">
                                <a href="/dashtraining/trainingdone" class="btn btn-sm btn-info">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header p-0 position-relative mt-n4 mt-3 mx-3 z-index-2">
                    <div class="bg-gradient-warning shadow-light border-radius-lg pt-4 pb-3">
                        <h3 class="text-black text-capitalize ps-3" align="center">Training Recapitulation
                        </h3>
                        <p class="text-white text-capitalize ps-3" align="center">All Recapitulation of Employee Training in
                            Kita Monster Digital
                        </p>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-4">
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr>
                                    <td>Minimum date:</td>
                                    <td><input type="text" id="min" name="min"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-4">
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr>
                                    <td>Maximum date:</td>
                                    <td><input type="text" id="max" name="max"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="col-4">
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr>
                                    <td>Status :</td>
                                    <td>
                                        <select id="status" class="form-select" aria-label="Default select example"
                                            name="approval_id">
                                            <option selected> All </option>
                                            <option value="1">
                                                Submitted
                                            <option value="2" selected>
                                                Accepted
                                            <option value="3">
                                                Rejected
                                            <option value="4">
                                                Done
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> --}}

                </div>
                <div class="card-body px-0 pb-0 pt-3">


                    {{-- <div class="row" align="right" style="margin-right:2%">
                        <div class="col-12 text-right">
                            <a href="/trainingdownload_pdf" class="btn btn-sm btn-light" target="_blank">Download</a>
                            <a href="/printadmin" class="btn btn-sm btn-light">Print All</a>
                            <a href="/printadminacc" class="btn btn-sm btn-light">Print Acc</a>
                        </div>
                    </div> --}}
                    <div class="card-body table-responsive pt-1">
                        <table id="myTable" class="table table-hover">
                            <thead class="text-warning">
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    No</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Name</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Role</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Training</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Start Date</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    End Date</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Status</th>
                            </thead>
                            <tbody>
                                @foreach ($alltrainings as $trn)
                                    <tr>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $trn->user->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $trn->user->position->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $trn->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">
                                                {{ date('d F Y', strtotime($trn->first_date)) }}
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">
                                                {{ date('d F Y', strtotime($trn->last_date)) }}</p>
                                        </td>
                                        {{-- @if ($trn->Approval->name == 'rejected') --}}
                                        <td>
                                            @if ($trn->Approval->name == 'rejected')
                                                <span class="badge bg-gradient-danger">
                                                    {{ $trn->Approval->name }}
                                                </span>
                                            @elseif($trn->Approval->name == 'accepted')
                                                <span class="badge bg-gradient-success">{{ $trn->Approval->name }}</span>
                                            @elseif($trn->Approval->name == 'submitted')
                                                <span class="badge bg-gradient-warning">{{ $trn->Approval->name }}</span>
                                            @elseif($trn->Approval->name == 'done')
                                                <span class="badge bg-gradient-info">{{ $trn->Approval->name }}</span>
                                            @endif
                                            {{-- <p class="text-xs font-weight-bold mb-0 b">{{ $trn->Approval->name }}</p> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="card mt-5">
                <div class="card-header p-0 position-relative mt-n4 mt-3 mx-3 z-index-2">
                    <div class="bg-gradient-warning shadow-light border-radius-lg pt-4 pb-3">
                        <h3 class="text-black text-capitalize ps-3" align="center">Training Organizer
                        </h3>
                        <p class="text-white text-capitalize ps-3" align="center">All Recapitulation Training Organizer in
                            Kita Monster Digital
                        </p>
                    </div>
                </div>
                <div class="card-body px-0 pb-0 pt-3">

                    {{-- <div class="row" align="right" style="margin-right:2%">
                        <div class="col-12 text-right">
                            <a href="/trainingdownload_pdf" class="btn btn-sm btn-light" target="_blank">Download</a>
                            <a href="/printadmin" class="btn btn-sm btn-light">Print All</a>
                            <a href="/printadminacc" class="btn btn-sm btn-light">Print Acc</a>
                        </div>
                    </div> --}}
                    <div class="card-body table-responsive pt-1">
                        <table id="myTable" class="table table-hover">
                            <thead class="text-warning">
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    No</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Company Training</th>
                                <th class="text-uppercase text-warning text-xs font-weight-bolder opacity-20">
                                    Total Training</th>
                            </thead>
                            <tbody>

                                @foreach ($company as $com)
                                    <tr>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $com->name }}</p>
                                        </td>
                                        <td>
                                            {{-- {{ dd($companyGet) }} --}}
                                            {{-- @foreach ($dataJumlah as $data) --}}
                                            <p class="text-xs font-weight-bold mb-0">
                                                {{ $com->applytraining->count() }}
                                            </p>
                                            {{-- @endforeach --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script> --}}
    @endsection

    @push('scripts')
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
        <script
            src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.jshttps://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js">
        </script>
        <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
        <script src="https://cdn.datatables.net/datetime/1.1.2/js/dataTables.dateTime.min.js"></script>
        {{-- <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> --}}
        <script>
            var minDate, maxDate;

            // Custom filtering function which will search data in column four between two values
            $.fn.dataTable.ext.search.push(
                function(settings, data, dataIndex) {
                    var min = minDate.val();
                    var max = maxDate.val();
                    var date = new Date(data[4]);
                    var dateAkhir = new Date(data[5]);
                    // var statusselect = $('select[name=approval_id] option').filter(':selected').val();

                    // console.log(statusselect)
                    // console.log(statustraining)

                    if (
                        (min === null && max === null) ||
                        (min === null && dateAkhir <= max) ||
                        (min <= date && max === null) ||
                        (min <= date && dateAkhir <= max)
                    ) {
                        return true;
                    }
                    return false;
                }
            );
            $(document).ready(function() {
                // Create date inputs
                minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });


                var table = $('#myTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'pdf', 'print'
                    ]
                });
                // $('#myTable').DataTable({

                // });
                $('#min, #max, #status').on('change', function() {
                    table.draw();
                });
            });
        </script>
    @endpush
