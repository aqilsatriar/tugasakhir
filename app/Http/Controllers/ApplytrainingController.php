<?php

namespace App\Http\Controllers;

use App\Models\Applytraining;
use App\Models\Company;
use App\Models\Position;
use App\Models\Approval;
use Illuminate\Contracts\Support\ValidatedData;
use Illuminate\Http\Request;

class ApplytrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('usertraining.applytraining.create', [
            'judul' => "Apply Training",
            'tampilCompany' => Company::all(),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usertraining.applytraining.create', [
            'judul' => "Apply Training",
            'tampilCompany' => Company::all(),
            'approval' => Approval::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $validatedData = $request->validate([

    //         'name' => 'required|max:255',
    //         'company' => 'required', 'string', 'max:200',
    //         'deskripsi' => 'required',
    //         'first_date' => 'required',
    //         'last_date' => 'required',
    //         'prove_pic' => 'image|file|max:1024',
    //         'price' => 'required',
    //         'location' => 'required',
    //         'link' => 'required|url',
    //         'approval_id' => '',
    //         'note' => ''
    //     ]);
    //     // dd($validatedData);

    //     if ($request->file('prove_pic')) {
    //         $validatedData['prove_pic'] = $request->file('prove_pic')->store('data-applytraining');
    //     }

    //     $validatedData['user_id'] = auth()->user()->id;
    //     Applytraining::create($validatedData);

    //     return redirect('/applytraining')->with('success', 'Ok');
    // }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'company_id' => '',
            'deskripsi' => 'required',
            'first_date' => 'required',
            'last_date' => 'required',
            'prove_pic' => 'required',
            'price' => 'required',
            'location' => 'required',
            'link' => 'required|url',
            'approval_id' => '',
            'note' => '',
            'certificate' => ''
        ]);

        if (isset($request->prove_pic)) {
            $extention = $request->prove_pic->extension();
            $file_name = time() . '.' . $extention;
            $txtprove = "storage/pictureTraining/" . $file_name;
            $request->prove_pic->storeAs('pictureTraining', $file_name);
        } else {
            $txtprove = null;
        }

        if (isset($request->certificate)) {
            $extention = $request->certificate->extension();
            $file_name = time() . '.' . $extention;
            $txtcertif = "storage/pictureCertificate/" . $file_name;
            $request->certificate->storeAs('pictureCertificate', $file_name);
        } else {
            $txtcertif = null;
        }
        // $request['user_id'] = auth()->user()->id;

        Applytraining::create([
            'user_id' => auth()->id(),
            'name' => $request->name,
            'company_id' => $request->company_id,
            'deskripsi' => $request->deskripsi,
            'prove_pic' => $txtprove,
            'first_date' => $request->first_date,
            'last_date' => $request->last_date,
            'price' => $request->price,
            'location' => $request->location,
            'link' => $request->link,
            'approval_id' => $request->approval_id,
            'note' => $request->note,
            'certificate' => $txtcertif
        ]);

        return redirect('/applytraining/create')->with('success', 'Training Has Been Submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function show(Applytraining $applytraining)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function edit(Applytraining $applytraining)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applytraining $applytraining)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applytraining $applytraining)
    {
        //
    }

    public function addCompany(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Company::create([
            'name' => $request->name,
        ]);

        return redirect('/applytraining/create')->with('successcompany', 'Company Has Been Created');
    }
}