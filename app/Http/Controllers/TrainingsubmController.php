<?php

namespace App\Http\Controllers;

use App\Models\Applytraining;
use App\Models\Approval;
use App\Models\Position;
use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrainingsubmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idUser = Auth::user()->id;
        // dd(Applytraining::getApproval($idUser));
        return view('usertraining.trainingsubm.index', [
            'judul' => "Training Submission",
            // 'approval' => Approval::all(),
            // 'user' => User::all(),
            'data' => Applytraining::getApproval($idUser)->where('approval_id', '=', '1')

            // 'data' => Applytraining::all(),
            // 'approval' => Approval::all(),
            // 'user' => User::all(),
            // 'position' => Position::all(),
            // 'roles' => Roles::all()

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd(Applytraining::getApprovalAdmin());

        $applytraining = Applytraining::find($id);
        // dd($applytraining);
        return view('usertraining.trainingsubm.show', [

            'applytraining' => $applytraining,
            'judul' => "Training Submission Details",

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function edit(Applytraining $applytraining)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applytraining $applytraining)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Applytraining  $applytraining
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applytraining $applytraining)
    {
        //
    }
}