<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Applytraining;
use App\Models\Approval;
use App\Models\Roles;
use App\Models\User;
use App\Models\Position;

class TrainingApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Applytraining::getApprovalAdmin());
        return view('admin.trainingapproval.index', [
            'judul' => "Training Submission",
            // 'data' => Approval::detailTraining($idUser),
            'data' => Applytraining::where('approval_id', '=', '1')->get(),
            // 'data' => Applytraining::getApprovalAdmin()
            //     ->where('approval_id', '=', '1'),
            'approval' => Approval::all(),
            'user' => User::all(),
            'position' => Position::all(),
            'roles' => Roles::all()

        ]);
    }

    public function show($id)
    {
        // dd(Applytraining::getApprovalAdmin());

        $applytraining = Applytraining::find($id);
        // dd($applytraining);
        return view('admin.trainingapproval.details', [

            'applytraining' => $applytraining,
            'judul' => "Training Submission Details",
            // 'data' => Approval::detailTraining($idUser),
            // 'data' => Applytraining::all(),
            // 'approval' => Approval::all(),
            // 'user' => User::all(),
            // 'position' => Position::all(),
            // 'roles' => Roles::all()

        ]);
    }

    public function statusAccept($id_apply)
    {
        Applytraining::where('id', $id_apply)->update(['approval_id' => 2]);
        return redirect('/trainingapproval')->with('success', 'Training has been accepted');
    }
    public function statusDeny($id_apply)
    {

        Applytraining::where('id', $id_apply)->update(['approval_id' => 3]);
        return redirect('/trainingapproval')->with('rejected', 'Training has been rejected');
    }

    public function addNote($id, Request $request)
    {
        // $validatedNote = Applytraining::all()->first();
        $validatedNote = Applytraining::findOrFail($id);
        // dd($validatedNote);
        $validatedNote = $request->validate([
            'note' => 'required'
        ]);

        Applytraining::where('id', $id)->update($validatedNote);

        return redirect('/trainingapproval')->with('addnote', 'Note has been created!');
    }
}