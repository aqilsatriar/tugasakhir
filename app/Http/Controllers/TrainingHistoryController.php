<?php

namespace App\Http\Controllers;

use App\Models\Applytraining;
use App\Models\Approval;
use App\Models\Position;

use Illuminate\Http\Request;

class TrainingHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.traininghistory.index', [
            'judul' => "Training History",
            'data' => Applytraining::getApprovalAdmin()
                ->where('approval_id', '=', '2')
            // 'approval' => Approval::all(),
            // 'position' => Position::all(),

        ]);
    }

    public function show($id)
    {
        // dd(Applytraining::getApprovalAdmin());

        $applytraining = Applytraining::find($id);
        // dd($applytraining);
        return view('admin.traininghistory.details', [

            'applytraining' => $applytraining,
            'judul' => "Training History Details",
            // 'data' => Approval::detailTraining($idUser),
            // 'data' => Applytraining::all(),
            // 'approval' => Approval::all(),
            // 'user' => User::all(),
            // 'position' => Position::all(),
            // 'roles' => Roles::all()

        ]);
    }
}