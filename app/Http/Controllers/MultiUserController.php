<?php

namespace App\Http\Controllers;

use App\Models\Applytraining;
use index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Roles;
use App\Models\Position;
use App\Models\Approval;
use App\Models\Company;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;


class MultiUserController extends Controller
{


    public function index()
    {
        $trainings = Applytraining::where('approval_id', '1')->count();
        $trainingsAcc = Applytraining::where('approval_id', '2')->count();
        $trainingsReject = Applytraining::where('approval_id', '3')->count();
        $trainingsDone = Applytraining::where('approval_id', '4')->count();


        $officers = User::count();
        $admin = User::where('roles_id', '1')->count();
        $employee = User::where('roles_id', '2')->count();

        $company = Company::withCount('applytraining')->get();

        // $countCompany = Applytraining::select('company_id')->get();

        //dd($countCompany);
        // dd($companyID);
        // $dataJumlah = [];
        // foreach ($countCompany as $data) {
        //     // dd($data);
        //     $jumlah = Applytraining::where('company_id', $data->company_id)->count();
        //     array_push($dataJumlah, $jumlah);
        // }
        // dd($dataJumlah);

        // $dataJumlah.array_push()
        // $companyTrainings = Applytraining::where('company_id', $companyID)->count();

        $userID = Auth::user()->id;

        $apvTrainings = Applytraining::where('approval_id', '1')->where('user_id', $userID)->count();
        $totalTrainings = Applytraining::where('approval_id', '4')->where('user_id', $userID)->count();
        // $count = DB::applytrainings('sub_category')->where('user_id', $id)->count();

        $role = Auth::user()->roles_id;
        if ($role == '1') {
            return view(
                'Adminn',
                compact('trainings', 'officers', 'admin', 'trainingsAcc', 'trainingsReject', 'trainingsDone'),
                [
                    'judul' => "Dashboard Admin",
                    'alltrainings' => Applytraining::all(),
                    'position' => Position::all(),
                    // 'dataJumlah' => $dataJumlah,
                    'company' => Company::all(),
                    'companyGet' => DB::table('applytrainings')->Join('company', function ($join) {
                        $join->on('applytrainings.company_id', '=', 'company.id');
                    })
                        ->select('company.name')->get()
                ]
            );
        }

        if ($role == '2') {
            return view(
                'Officer',
                compact('apvTrainings', 'totalTrainings'),
                [
                    'judul' => "Dashboard Officer",
                    'trainingsofc' => Applytraining::all()->where('user_id', $userID)
                ]
            );
        }

        if ($role == '3') {
            return view(
                'director.index',
                compact('trainings', 'officers', 'admin', 'employee', 'trainingsAcc', 'trainingsReject', 'trainingsDone'),
                [
                    'judul' => "Dashboard Director",
                ]
            );
        } else {
            return view('login');
        }
    }


    public function print_adminAll()
    {
        return view('printadmin', [
            'trainings' => Applytraining::getApprovalAdmin()
        ]);
    }

    public function print_adminAcc()
    {
        return view('printadminacc', [
            'trainings' => Applytraining::getApprovalAdmin()->where('approval_id', '=', '2'),
        ]);
    }

    public function dashSubmitted()
    {
        return view('admin.dashtraining.trainingsubmission', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '1'),
            'judul' => "Training Submission",
            'position' => Position::all()
        ]);
    }

    public function dashAccepted()
    {
        return view('admin.dashtraining.trainingacc', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '2'),
            'judul' => "Training Accepted",
            'position' => Position::all()

        ]);
    }

    public function dashRejected()
    {
        return view('admin.dashtraining.trainingreject', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '3'),
            'judul' => "Training Rejected",
            'position' => Position::all()
        ]);
    }

    public function dashDone()
    {
        return view('admin.dashtraining.trainingdone', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '4'),
            'judul' => "Training Finished",
            'position' => Position::all()
        ]);
    }

    public function tampilSubmitted()
    {
        return view('director.training.trainingsubmission', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '1'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }

    public function tampilAccepted()
    {
        return view('director.training.trainingacc', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '2'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }
    public function tampilRejected()
    {
        return view('director.training.trainingreject', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '3'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }

    public function tampilDone()
    {
        return view('director.training.trainingdone', [
            'alltrainings' => Applytraining::all()->where('approval_id', '=', '4'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }

    public function tampilOfficer()
    {
        return view('director.dataofficer.officer', [
            'allofficer' => User::all(),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }


    public function tampilAdmin()
    {
        return view('director.dataofficer.admin', [
            'alladmin' => User::all()->where('roles_id', '=', '1'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }

    public function tampilEmployee()
    {
        return view('director.dataofficer.employee', [
            'allemployee' => User::all()->where('roles_id', '=', '2'),
            'judul' => "Dashboard Director",
            'position' => Position::all()
        ]);
    }

    public function download_pdf()
    {
        $trainings = Applytraining::getApprovalAdmin();
        $pdf = PDF::loadview('trainingdownload_pdf', ['trainings' => $trainings]);
        return $pdf->download('training-data-pdf');
    }
}