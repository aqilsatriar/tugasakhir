<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Roles;
use App\Models\Position;
use Barryvdh\DomPDF\Facade\Pdf;
// use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;

class OfficerdataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user = User::tampilTabel();
        // dd($user);

        return view('admin.officerdata.index', [
            'judul' => "Officer Data",
            'user' => User::tampilTabel(),
            'tampilPosition' => Position::all(),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     return view('dashboard.user.create', [
    //         'judul' => "Officer Data",
    //         'user' => User::all(),
    //         'roles' => Roles::all()
    //     ]);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'roles_id' => 'required',
            'positions_id' => '',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'address' => 'required',
            'place_date' => 'required',
            'birth_date' => 'required',
            'join_date' => 'required',
            'picture' => 'required',
            'identity_picture' => 'required'
        ]);

        // if ($request->file('picture')) {
        //     $validatedData['picture'] = $request->file('picture')->store('data-officer');
        // }

        // if ($request->file('identity_picture')) {
        //     $validatedData['identity_picture'] = $request->file('identity_picture')->store('data-officer');
        // }

        if (isset($request->picture)) {
            $extention = $request->picture->extension();
            $file_name = time() . '.' . $extention;
            $txt = "storage/picture/" . $file_name;
            $request->picture->storeAs('picture', $file_name);
        } else {
            $txt = null;
        }

        if (isset($request->identity_picture)) {
            $extention = $request->identity_picture->extension();
            $file_name = time() . '.' . $extention;
            $txtidentity = "storage/identity_picture/" . $file_name;
            $request->identity_picture->storeAs('identity_picture', $file_name);
        } else {
            $txtidentity = null;
        }
        // $validatedData['password'] = Hash::make($validatedData['password']);

        // User::create($validatedData);

        User::create([
            'roles_id' => $request->roles_id,
            'positions_id' => $request->positions_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'place_date' => $request->place_date,
            'birth_date' => $request->birth_date,
            'join_date' => $request->join_date,
            'picture' => $txt,
            'identity_picture' => $txtidentity
        ]);

        return redirect('/officerdata')->with('success', 'Officer Data Has Been Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.officerdata.show', [
            'judul' => "Officer Data Details",
            'user' => $user,
            'roles' => Roles::all(),
            'tampilPosition' => Position::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        // return ($user);
        return view('admin.officerdata.edit', [
            'judul' => "Officer Data Edit",
            'user' => $user,
            'tampilPosition' => Position::all(),
            'roles' => Roles::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $validatedData = $request->validate([
    //         'roles_id' => 'required',
    //         'positions_id' => '',
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'password' => 'required',
    //         'address' => 'required',
    //         'place_date' => 'required',
    //         'birth_date' => 'required',
    //         'join_date' => 'required',
    //         'picture' => 'image|file|max:1024',
    //         'identity_picture' => 'image|file|max:1024'
    //     ]);
    //     $validatedData['password'] = Hash::make($validatedData['password']);

    //     User::where('id', $id)->update($validatedData);

    //     return redirect('/officerdata')->with('success', 'Success !');
    // }

    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->address = $request->address;
        $user->place_date = $request->place_date;
        $user->birth_date = $request->birth_date;
        $user->join_date = $request->join_date;
        if (isset($request->picture)) {
            $extention = $request->picture->extension();
            $file_name = time() . '.' . $extention;
            $txt = "storage/picture/" . $file_name;
            $request->picture->storeAs('picture', $file_name);
            $user->picture = $txt;
        } else {
            $txt = null;
        }

        if (isset($request->identity_picture)) {
            $extention = $request->identity_picture->extension();
            $file_name = time() . '.' . $extention;
            $txtidentity = "storage/identity_picture/" . $file_name;
            $request->identity_picture->storeAs('identity_picture', $file_name);
            $user->identity_picture = $txtidentity;
        } else {
            $txtidentity = null;
        }
        $user->save();
        return redirect('/officerdata')->with('success', 'Officer Data has been edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect('/officerdata');
    }

    public function cetak_pdf()
    {
        $user = User::tampilTabel();
        $pdf = PDF::loadview('admin.officerdata.download_pdf', ['user' => $user]);
        return $pdf->download('officer-data-pdf');
    }

    public function print_pdf()
    {
        $user = User::tampilTabel();
        return view('admin.officerdata.print_pdf', ['user' => $user]);
    }


    // public function TampilData()
    // {
    //     // dd(Position::all());
    //     // dd(Auth::user());
    //     return view('admin.officerdata.index', ['tampilPosition' => Position::all(), 'user' => Auth::user()]);
    // }
}