<?php

namespace App\Http\Controllers;

use App\Models\Applytraining;
use App\Models\Approval;
use App\Models\Position;
use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OFTrainingHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idUser = Auth::user()->id;
        return view('usertraining.oftraininghistory.index', [
            'judul' => "Training History",
            'data' => Applytraining::getApproval($idUser),
            // 'data' => Applytraining::where('approval_id', '=', '2')
            //     ->orWhere('approval_id', '=', '3')->getApproval($idUser),

            // 'data' => Applytraining::where('approval_id', '=', '2')
            //     ->orWhere('approval_id', '=', '3')->get(),
            // 'data' => Applytraining::all(),
            'approval' => Approval::all(),
            'user' => User::all(),
            'position' => Position::all(),
            'roles' => Roles::all()
        ]);
    }


    public function show($id_show)
    {
        //dd($id_show);

        $idUser = Auth::user()->id;
        return view('usertraining.oftraininghistory.print', [
            'data' => Applytraining::getApproval($idUser)->where('id', $id_show),
        ]);
    }

    // public function statusDone($id_apply)
    // {

    //     // return redirect('/oftraininghistory')->with('success', 'Congratulations, Training is Done!');
    // }

    public function updateCertificate(Request $request, $id)
    {
        $user = Applytraining::findOrfail($id);
        if (isset($request->certificate)) {
            $extention = $request->certificate->extension();
            $file_name = time() . '.' . $extention;
            $txtcertif = "storage/certificate/" . $file_name;
            $request->certificate->storeAs('certificate', $file_name);
            $user->certificate = $txtcertif;
        } else {
            $txtcertif = null;
        }

        $user->save();
        $user->update(['approval_id' => 4]);
        return redirect('/oftraininghistory')->with('success', 'Certificate has benn uploaded');
    }

    // public function addCertificate($id, Request $request)
    // {
    //     // $validatedNote = Applytraining::all()->first();
    //     $validatedCertif = Applytraining::findOrFail($id);
    //     // dd($validatedNote);
    //     $validatedCertif = $request->validate([
    //         'certificate' => 'required'
    //     ]);


    //     Applytraining::where('id', $id)->update($validatedCertif);
    //     $this->statusDone($id);
    //     return redirect('/oftraininghistory')->with('addnote', 'Note has been created!');
    // }
}