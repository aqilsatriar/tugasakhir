<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.company.index', [
            'company' => Company::all(),
            'judul' => "Training Company",
        ]);
    }

    public function indexofc()
    {
        return view('usertraining.companyofc.index', [
            'company' => Company::all(),
            'judul' => "Training Company",
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create', [
            'judul' => "Add Training Company",
        ]);
    }

    public function createofc()
    {
        return view('usertraining.companyofc.create', [
            'judul' => "Add Training Company",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        Company::create($validatedData);

        return redirect('/company')->with('success', 'Ok');
    }

    public function storeofc(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required'
        ]);

        Company::create($validatedData);

        return redirect('/companyofc')->with('success', 'Ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('admin.company.show', [
            'company' => $company
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.edit', [
            'company' => $company
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $rules = [
            'name' => 'required'
        ];

        $validatedData = $request->validate($rules);

        Company::where('id', $company->id)
            ->update($validatedData);

        return redirect('/company')->with('success', 'Ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        Company::destroy($company->id);
        return redirect('/company')->with('success', ' Berhasil Dihapus');
    }
}