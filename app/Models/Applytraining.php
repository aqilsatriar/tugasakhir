<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Applytraining extends Model
{
    use HasFactory;

    protected $table = 'applytrainings';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // public function company()
    // {
    //     return $this->belongsTo(Company::class, 'company_id', 'id'); 
    // }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'positions_id', 'id');
    }

    public static function getApproval($idUser)
    {
        return DB::table('applytrainings')

            ->join('approvals', 'applytrainings.approval_id', '=', 'approvals.id')
            ->join('users', 'applytrainings.user_id', '=', 'users.id')
            ->join('company', 'applytrainings.company_id', '=', 'company.id')
            ->join('position', 'users.positions_id', '=', 'position.id')
            ->select(
                'applytrainings.name as applyname',
                'applytrainings.id as id',
                'users.positions_id as usposid',
                'position.id as posid',
                'user_id',
                'location',
                'price',
                'company.name as company',
                'deskripsi',
                'first_date',
                'last_date',
                'prove_pic',
                'note',
                'link',
                'approval_id',
                'position.name as posname',
                'approvals.name',
                'note',
                'users.name as username',
                'email'
            )
            ->where('applytrainings.user_id', $idUser)
            ->get();
    }

    // public static function tampilData($idUser)
    // {
    //     return DB::table('applytrainings')
    //         ->select("*")
    //         // ->join('approvals', 'applytrainings.approval_id', '=', 'approvals.id')
    //         ->where('applytrainings.user_id', $idUser)
    //         ->get();
    // }

    // public static function getApprovalAdmin()
    // {
    //     return DB::table('applytrainings')
    //         ->select("*")
    //         ->join('users', 'applytrainings.user_id', '=', 'users.id')
    //         ->join('approvals', 'applytrainings.approval_id', '=', 'approvals.id')
    //         ->get();
    // }

    public static function getApprovalAdmin()
    {
        return DB::table('applytrainings')
            ->join('users', 'applytrainings.user_id', '=', 'users.id')
            ->join('company', 'applytrainings.company_id', '=', 'company.id')
            ->join('approvals', 'applytrainings.approval_id', '=', 'approvals.id')
            ->join('position', 'users.positions_id', '=', 'position.id')
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->select(
                // 'users.id as id',
                'applytrainings.name as applyname',
                'applytrainings.id as trainingId',
                'applytrainings.user_id as usId',
                'roles.id as rolesId',
                'approvals.id as id',
                'position.id as posid',
                'location',
                'price',
                'company.name as company',
                'deskripsi',
                'first_date',
                'last_date',
                'prove_pic',
                'note',
                'link',
                'approval_id',
                'position.name as posname',
                'approvals.name',
                'users.name as username',
                'roles',
                'applytrainings.note as trainingsnote',
                'email'
            )
            ->get();
    }
}