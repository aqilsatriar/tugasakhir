<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'company';
    protected $fillable = [
        'name',
    ];

    public function applytraining()
    {
        return $this->hasMany(Applytraining::class, 'company_id', 'id');
    }
}