<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'roles_id',
        'positions_id',
        'address',
        'place_date',
        'birth_date',
        'join_date',
        'picture',
        'identity_picture',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsTo(Roles::class, 'roles_id', 'id');
    }

    public function applytraining()
    {
        return $this->hasMany(Applytraining::class, 'user_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'positions_id', 'id');
    }

    public static function tampilTabel()
    {
        return DB::table('users')
            ->join('position', 'users.positions_id', '=', 'position.id')
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->select(
                'users.name as username',
                'users.id as usId',
                'roles.id as rolId',
                'users.roles_id as rolUsId',
                'users.positions_id as usposId',
                'roles',
                'email',
                'address',
                'place_date',
                'birth_date',
                'join_date',
                'picture',
                'identity_picture',
                'position.id as posId',
                'position.name as posname'
            )
            ->get();
    }
}