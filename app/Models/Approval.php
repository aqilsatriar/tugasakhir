<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Approval extends Model
{
    use HasFactory;

    protected $table = 'approvals';

    public function applytraining() //relasi tabel
    {

        return $this->hasMany(Applytraining::class, 'approve_id', 'id');
    }

    public static function detailTraining($idUser)
    {
        return DB::table('applytrainings')
            ->select("*")
            // ->join('approvals', 'applytrainings.approval_id', '=', 'approvals.id')
            ->where('applytrainings.user_id', $idUser)
            ->get();
    }
}